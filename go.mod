module gitlab.com/eclaude/covid-19

go 1.14

require (
	github.com/dustin/go-humanize v1.0.0
	github.com/fatih/color v1.9.0
	github.com/leekchan/accounting v0.0.0-20191218023648-17a4ce5f94d4
	github.com/olekukonko/tablewriter v0.0.4
)
