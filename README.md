[![Go Report Card](https://goreportcard.com/badge/github.com/wayneashleyberry/ncov)](https://goreportcard.com/report/gitlab.com/eclaude/covid-19)

#COVID-19

Simple CLI tool to display statistics from https://covid19stats.live

### Installation

```sh
go get gitlab.com/eclaude/covid-19
```

### Usage

```sh
covid-19
```