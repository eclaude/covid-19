package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/dustin/go-humanize"
	"github.com/fatih/color"
	"github.com/olekukonko/tablewriter"
)

var (
	inCountry string
)

func init() {
	flag.StringVar(&inCountry, "c", "RE,TH,FR,DE,US,CN,AU,IT", "Country codes like : RE,TH,FR,DE,US,CN,AU")
	flag.Parse()
}

func main() {
	err := run()
	if err != nil {
		panic(err)
	}
}

func run() error {
	c := &http.Client{
		Timeout: time.Second * 10,
	}

	const baseURL = "https://exchange.vcoud.com/coronavirus/latest"

	req, err := http.NewRequest(http.MethodGet, baseURL, nil)
	if err != nil {
		return err
	}

	resp, err := c.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("bad status code")
	}

	type items []struct {
		ID             string    `json:"_id"`
		Name           string    `json:"name"`
		TotalCases     float64   `json:"totalCases"`
		TotalDeaths    float64   `json:"totalDeaths"`
		SeriousCases   float64   `json:"seriousCases"`
		TotalRecovered float64   `json:"totalRecovered"`
		TotalCases24H  float64   `json:"totalCases24h"`
		TotalDeaths24H float64   `json:"totalDeaths24h"`
		CreatedAt      time.Time `json:"createdAt"`
		UpdatedAt      time.Time `json:"updatedAt"`
		Slug           string    `json:"slug,omitempty"`
		Symbol         string    `json:"symbol,omitempty"`
		Population     float64   `json:"population,omitempty"`
		Region         string    `json:"region,omitempty"`
		Subregion      string    `json:"subregion,omitempty"`
	}

	var r items

	err = json.Unmarshal(body, &r)
	if err != nil {
		return err
	}

	countryCodes := strings.Split(inCountry, ",")

	var t time.Time
	table := tablewriter.NewWriter(os.Stdout)
	white := color.New(color.FgWhite).SprintFunc()
	yellow := color.New(color.FgYellow).SprintFunc()
	green := color.New(color.FgGreen).SprintFunc()
	red := color.New(color.FgRed).SprintFunc()
	magenta := color.New(color.FgMagenta).SprintFunc()

	table.SetHeader([]string{"🌍", "😷", "💀", "🥳", "📊"})
	for _, item := range r {
		for _, code := range countryCodes {
			if item.Symbol == code {
				data := []string{
					white(item.Name),
					yellow(item.TotalCases),
					red(item.TotalDeaths),
					green(item.TotalRecovered),
					magenta(fmt.Sprintf("%.2f%%", (item.TotalRecovered/item.TotalCases)*100)),
				}
				table.Append(data)
			}
		}
		t = item.UpdatedAt
	}
	table.Render()
	color.Blue("Updated\t %s\n", humanize.Time(t))

	return nil
}
